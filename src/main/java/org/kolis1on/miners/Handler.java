package org.kolis1on.miners;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.kolis1on.miners.menu.MenuCreate;
import org.kolis1on.miners.objects.Mine;
import org.kolis1on.miners.storage.Storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class Handler implements Listener {

    public Storage storage = Storage.getInstance();
    private final Economy econ = Miners.getEconomy();
    public Miners plugin;
    public Handler(Miners plugin){
        this.plugin = plugin;
    }
    public static HashMap<Player, Inventory> open = new HashMap<Player, Inventory>();
    @EventHandler
    public void mineInteract(PlayerInteractEvent e){
            Player p = e.getPlayer();

            if(e.getAction() == Action.RIGHT_CLICK_BLOCK){
                Location mineLoc = e.getClickedBlock().getLocation();
                if(e.getClickedBlock().getType() == Material.CHEST){
                    if(storage.getMine(mineLoc.getX(), mineLoc.getY(), mineLoc.getZ())){
                        Mine mine = Mine.mines.get(mineLoc);
                        Inventory inv = mine.getInventory();
                        fastPlaceholderChange(mineLoc, inv);
                        p.openInventory(inv);
                        open.put(p, inv);
                        e.setCancelled(true);
                    }

                }
            }

    }

    @EventHandler
    public void closeInventory(InventoryCloseEvent e){
        MenuCreate.mainInventories.forEach((loc, inv)->{
            if(e.getInventory().equals(inv)){
                open.remove(e.getPlayer());
            }
        });

    }


    @EventHandler
    public void breakBlock(BlockBreakEvent e){
        Location mineLoc = e.getBlock().getLocation();
        if(storage.getMine(mineLoc.getX(), mineLoc.getY(), mineLoc.getZ())){
            e.setCancelled(true);
        }

    }
    boolean check = false;
    @EventHandler
    public void inventoryClick(InventoryClickEvent e){
        Player p = (Player) e.getWhoClicked();
        MenuCreate.mainInventories.forEach((loc, inv)->{
            if(e.getInventory().equals(inv)){
                System.out.println("eq");
                Mine mine = Mine.mines.get(loc);
                if(e.getSlot() == 21 && mine.getTime() == 0){
                    System.out.println("ok");
                    System.out.println(mine);
                    mine.setTime(this.plugin.getConfig().getInt("time"));
                    System.out.println(mine.getTime());
                    fastPlaceholderChange(loc, inv);
                    giveAward(loc, inv, p);

                }
                if(e.getSlot() == 23){
                    p.openInventory(MenuCreate.subInventories.get(e.getClickedInventory()));
                }
                e.setCancelled(true);
            }
        });


            MenuCreate.subInventories.forEach((mainInv, subInv) -> {
                if (e.getInventory().equals(subInv)) {

                        MenuCreate.mainInventories.forEach((loc, inv) -> {
                            if (mainInv == inv) {
                                Mine mine = Mine.mines.get(loc);
                                if(e.getSlot() == this.plugin.getConfig().getInt("menus.submenu.back.slot")){
                                    p.openInventory(MenuCreate.mainInventories.get(loc));
                                    open.put(p, MenuCreate.mainInventories.get(loc));
                                }
                                    if (e.getCurrentItem() != null && e.getCurrentItem().getType()
                                            .equals(Material.valueOf(this.plugin.getConfig().getString("menus.submenu.current.material")))) {
                                String name = mine.getName();
                                int level = mine.getLevel() + 1;

                                int money = plugin.getConfig().getInt("menus." + name + ".items." + level + ".build.money");
                                ConfigurationSection materials = this.plugin.getConfig().getConfigurationSection("menus." + name + ".items." + level + ".build.items");

                                int countS = 0;
                                int countItems = 0;
                                for (String line : materials.getKeys(false)) {
                                    countS++;
                                    ItemStack item = new ItemStack(Material.valueOf(line));
                                    if (p.getInventory().containsAtLeast(item, this.plugin.getConfig().getInt("menus." + name + ".items." + level + ".build.items." + line))) {
                                        countItems++;
                                    }
                                }
                                if (econ.getBalance(p) >= money && countS == countItems) {

                                    for (String line : materials.getKeys(false)) {

                                        int count = this.plugin.getConfig().getInt("menus." + name + ".items." + level + ".build.items." + line);
                                        ItemStack removable = new ItemStack(Material.valueOf(line), count);

                                        p.getInventory().removeItem(removable);
                                    }
                                    econ.withdrawPlayer(p, money);
                                    mine.setLevel(mine.getLevel() + 1);
                                    Inventory newInv = MenuCreate.subMenu(this.plugin, mine.getName(),  mine.getLevel() + 1);

                                    MenuCreate.subInventories.put(inv, newInv);
                                    p.openInventory(MenuCreate.subInventories.get(inv));
                                    return;


                             }

                            }
                                    }
                        });
                    e.setCancelled(true);
                }

            });


        }





    public void giveAward(Location loc, Inventory inv, Player p){
        Mine mine = Mine.mines.get(loc);
        int level = mine.getLevel();
        String name = mine.getName();

        List<String> commands = (List<String>) this.plugin.getConfig().getList("menus." + name+".items." +level + ".award.commands");
        commands = commands.stream().map(s -> s.replace("%player%", p.getName())).collect(Collectors.toList());
        for(String cmd: commands){
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd);
        }
    }

    public void fastPlaceholderChange(Location loc, Inventory inv){
        Mine mine = Mine.mines.get(loc);
        List<String> lore = (List<String>) this.plugin.getConfig().getList("menus.main.items.main_item.lore");

        int time = mine.getTime();

        int hours = time / 3600;
        int minutes = (time % 3600) / 60;
        int seconds = time % 60;

        String timeFormat = String.format("%02d:%02d:%02d", hours, minutes, seconds);


        lore = (ArrayList<String>) lore.stream().map(p -> p.replace("&", "\u00a7").
                replace("%time%", timeFormat).replace("%level%", String.valueOf(mine.getLevel()))).collect(Collectors.toList());

        ItemStack cloneItem = inv.getItem(21).clone();

        ItemMeta itemMeta = cloneItem.getItemMeta();
        itemMeta.setLore(lore);
        cloneItem.setItemMeta(itemMeta);

        inv.setItem(21, cloneItem);
    }

}

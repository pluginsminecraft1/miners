package org.kolis1on.miners.storage;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.kolis1on.miners.menu.MenuCreate;
import org.kolis1on.miners.objects.Mine;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Storage {

    private Connection connection;
    private File dbFile = null;
    private static Storage instance = null;
    private String dir;



    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }

        return instance;

    }


    public void createDB() {
        if (dbFile == null) {
            File db = new File(dir + File.separator + "/plugins/Miners/storage.db");
            try {
                db.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
           Storage.getInstance().dbFile = db;
        }

    }
    private Storage() {
        dir = System.getProperty("user.dir");
        try {
            this.connection = DriverManager.getConnection("jdbc:sqlite:" + dir + File.separator + "/plugins/Miners/storage.db");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private Connection getConnection() {

        return this.connection;
    }

    public void createTables() {
        try {

            Statement statement = getConnection().createStatement();

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS storage_blocks(name varchar(255), level int, time int, x double, y double, z double)");


        } catch (SQLException e) {

            e.printStackTrace();
        }


    }

    public void inputMine(String name, int time, double x, double y, double z) {


        try {
            Statement statement = getConnection().createStatement();

            statement.executeUpdate("INSERT INTO storage_blocks(`name`, `level`, `time`, `x`, `y`, `z`)"
                    + "VALUES ('" + name + "'," + 0 + "," + time + ","+ x + "," + y + "," + z + ")");
            statement.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }


    }

    public boolean getMine(double x, double y, double z) {
        try {
            Statement statement = getConnection().createStatement();
            ResultSet res = statement.executeQuery("SELECT * FROM `storage_blocks` WHERE `x` = " + x + " AND `y` = " + y + " AND `z` = " + z);
            if (res.next()) {
                return true;

            }

            statement.close();


        } catch (SQLException e) {

            e.printStackTrace();
        }


        return false;
    }


    public String getName(Location location) {
        try {
            Statement statement = getConnection().createStatement();
            ResultSet res = statement.executeQuery("SELECT `name` FROM `storage_blocks` WHERE `x` = " + location.getX() +
                    " AND `y` = " + location.getY() + " AND `z` = " + location.getZ());
            if (res.next()) {
                return res.getString("name");
            }


            statement.close();


        } catch (SQLException e) {

            e.printStackTrace();
        }

        return null;
    }

    public void loadMenus(JavaPlugin plugin) {
        try {
            Statement statement = getConnection().createStatement();
            ResultSet res = statement.executeQuery("SELECT * FROM `storage_blocks`");
            while (res.next()) {
                double x = res.getDouble("x");
                double y = res.getDouble("y");
                double z = res.getDouble("z");
                Location loc = new Location(Bukkit.getServer().getWorld("world"), x, y, z);
                System.out.println("Location " + loc);
                MenuCreate.createInv(loc, plugin);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    private HashMap<Location, Integer> getTime() {
        HashMap<Location, Integer> time = new HashMap<Location, Integer>();
        try {
            Statement statement = getConnection().createStatement();
            ResultSet res = statement.executeQuery("SELECT * FROM `storage_blocks`");
            while (res.next()) {
                double x = res.getDouble("x");
                double y = res.getDouble("y");
                double z = res.getDouble("z");

                Location loc = new Location(Bukkit.getServer().getWorld("word"),x,y,z);
                if(res.getInt("time") >= 1)
                time.put(loc, res.getInt("time"));
            }
            statement.close();


        } catch (SQLException e) {

            e.printStackTrace();
        }
        return time;
    }

    public void setTime() {
            getTime().forEach((location, time) ->{
                try {

                    Statement statement = getConnection().createStatement();
                    time--;
                    statement.executeUpdate("UPDATE `storage_blocks` SET `time` = "+ time+" WHERE `x` = " + location.getX() +
                            " AND `y` = " + location.getY() + " AND `z` = " + location.getZ());
                    statement.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });

        }
    public Integer getTimeOne(Location location) {
        try {
            Statement statement = getConnection().createStatement();
            ResultSet res = statement.executeQuery("SELECT `time` FROM `storage_blocks` WHERE `x` = " + location.getX() +" AND `y` = " + location.getY() + " AND `z` = " + location.getZ());
            if(res.next()){
                return res.getInt("time");
            }
            statement.close();


        } catch (SQLException e) {

            e.printStackTrace();
        }
        return 0;
    }
    public void setTimeOne(Location location, JavaPlugin plugin) {
        try {
            Statement statement = getConnection().createStatement();
            double x = location.getX();
            double y = location.getY();
            double z = location.getZ();

            statement.executeUpdate("UPDATE `storage_blocks` SET `time` = " + plugin.getConfig().getInt("time")+ " WHERE `x` = " + location.getX() +" AND `y` = " + location.getY() + " AND `z` = " + location.getZ());

            statement.close();


        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    public Integer getLevel(Location location) {
        try {
            Statement statement = getConnection().createStatement();
            ResultSet res = statement.executeQuery("SELECT `level` FROM `storage_blocks` WHERE `x` = " + location.getX() +" AND `y` = " + location.getY() + " AND `z` = " + location.getZ());
            if(res.next()){
                return res.getInt("level");
            }
            statement.close();


        } catch (SQLException e) {

            e.printStackTrace();
        }
        return 0;
    }

    public void setLevel(Location location) {
        try {
            Statement statement = getConnection().createStatement();
            double x = location.getX();
            double y = location.getY();
            double z = location.getZ();
            int level = getLevel(location) + 1;
            statement.executeUpdate("UPDATE `storage_blocks` SET `level` = " + level + " WHERE `x` = " + location.getX() +" AND `y` = " + location.getY() + " AND `z` = " + location.getZ());

            statement.close();


        } catch (SQLException e) {

            e.printStackTrace();
        }
    }
    public void reloadMines() {
        try {
            Statement statement = getConnection().createStatement();
            Mine.mines.forEach((location, mine) -> {
                double x = location.getX();
                double y = location.getY();
                double z = location.getZ();
                int level = mine.getLevel();
                System.out.println("1");
                if(getMine(x,y,z)){
                    System.out.println("f");
                    try {
                        statement.executeUpdate("UPDATE `storage_blocks` SET `name` = '"+ mine.getName() +
                                "', `level` = " + level + " ,`time` = "+mine.getTime()+", " +
                                "`x` = "+x+", `y` = "+y+", `z` = "+z+" WHERE `x` = " + x + " AND `y` = " + y + " AND `z` = " + z);
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                }else{
                    inputMine(mine.getName(), mine.getTime(), x,y,z);
                }

            });


        } catch (SQLException e) {

            e.printStackTrace();
        }
        }
    }

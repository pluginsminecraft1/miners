package org.kolis1on.miners;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.ServicesManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.kolis1on.miners.command.MineCommand;
import org.kolis1on.miners.menu.MenuCreate;
import org.kolis1on.miners.objects.Mine;
import org.kolis1on.miners.storage.Storage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public final class Miners extends JavaPlugin {

    public Storage storage = Storage.getInstance();
    private static Economy economy = null;
    @Override
    public void onEnable() {
        File config = new File(getDataFolder() + File.separator + "config.yml");
        if (!config.exists()) {
            getConfig().options().copyDefaults(true);
            saveDefaultConfig();
        }

        if (!setupEconomy()) {
            System.out.println("Vault not found!");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        ServicesManager sm = Bukkit.getServicesManager();
        sm.register(Economy.class, economy, getServer().getPluginManager().getPlugin("Vault"), ServicePriority.Lowest);


        getCommand("mine").setExecutor(new MineCommand(this));
        Bukkit.getPluginManager().registerEvents(new Handler(this), this);
        storage.createDB();
        storage.createTables();
        storage.loadMenus(this);

        this.getServer().getScheduler().runTaskTimerAsynchronously(this, () -> {
            Mine.mines.forEach((locMine, mine) ->{
                if (mine.getTime() > 0) {
                    mine.setTime(mine.getTime() - 1);
                }
            if (Handler.open != null) {
                Handler.open.forEach((player, inventory) -> {
                    MenuCreate.mainInventories.forEach((loc, inv) -> {
                        if (loc.equals(locMine)) {
                            ItemStack cloneItem = inv.getItem(21).clone();
                            List<String> lorex = this.getConfig().getStringList("menus.main.items.main_item.lore");
                            int time = mine.getTime();
                            if (time == 0) {
                                String takeAward = this.getConfig().getString("takeAward");
                                lorex = (ArrayList)lorex.stream().map((p) -> {
                                    return p.replace("&", "§").replace("%time%", takeAward).replace("%level%", String.valueOf(mine.getLevel()));
                                }).collect(Collectors.toList());
                            } else {
                                int hours = time / 3600;
                                int minutes = time % 3600 / 60;
                                int seconds = time % 60;
                                String timeFormat = String.format("%02d:%02d:%02d", hours, minutes, seconds);
                                System.out.println(timeFormat);
                                lorex = (ArrayList)lorex.stream().map((p) -> {
                                    return p.replace("&", "§").replace("%time%", timeFormat).replace("%level%", String.valueOf(mine.getLevel()));
                                }).collect(Collectors.toList());
                            }

                            ItemMeta itemMeta = cloneItem.getItemMeta();
                            itemMeta.setLore(lorex);
                            cloneItem.setItemMeta(itemMeta);
                            inv.setItem(21, cloneItem);
                        }

                    });
                });
            }

            });
        }, 0L, 20L);


    }
    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager()
                .getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }

    public static Economy getEconomy() {
        return economy;
    }

    @Override
    public void onDisable() {
        storage.reloadMines();
    }
}

package org.kolis1on.miners.command;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.kolis1on.miners.Miners;
import org.kolis1on.miners.menu.MenuCreate;
import org.kolis1on.miners.storage.Storage;

public class MineCommand implements CommandExecutor {

    public Miners plugin;
    public MenuCreate menus = MenuCreate.getInstance();
    public Storage storage = Storage.getInstance();
    public MineCommand(Miners plugin){
        this.plugin = plugin;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(sender instanceof Player){
            if(args.length == 1 && args[0].equalsIgnoreCase("reload")){
                if(sender.hasPermission("mine.reload")){
                    this.plugin.reloadConfig();
                    this.plugin.saveConfig();

                    storage.reloadMines();
                    storage.loadMenus(this.plugin);
                    sender.sendMessage("Плагин успешно перезагружен");
                }
            }
            Player player = (Player) sender;
            if(player.hasPermission("miners.command.create")){
                ConfigurationSection menuSection = this.plugin.getConfig().getConfigurationSection("menus");
                if(args.length == 2 && args[0].equalsIgnoreCase("create")){
                    for(String menu: menuSection.getKeys(false)){
                        if(!args[1].equalsIgnoreCase("main") && args[1].equalsIgnoreCase(menu)){
                            Block b = player.getTargetBlockExact(100);
                            Location loc = b.getLocation();
                            loc.setY(loc.getY() + 1);
                            storage.inputMine(args[1], this.plugin.getConfig().getInt("time"), loc.getX(), loc.getY(), loc.getZ());
                            MenuCreate.createInv(loc, this.plugin);
                            Bukkit.getServer().getWorld(loc.getWorld().getUID()).getBlockAt(loc).setType(Material.CHEST);

                            player.sendMessage("Шахта успешно создана");

                        }

                    }


                }
            }
        }
        return false;
    }
}

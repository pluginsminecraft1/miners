package org.kolis1on.miners.objects;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Getter
@Setter
public class Mine {
    public static Map<Location,Mine> mines = new HashMap<>();
    public int level;
    public Location location;
    public int time;
    public Inventory inventory;
    public String name;
    public Mine(int level, Location location, int time, Inventory inventory, String name){
        this.level = level;
        this.location = location;
        this.time = time;
        this.inventory = inventory;
        this.name = name;
    }

}
